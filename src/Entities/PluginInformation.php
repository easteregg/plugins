<?php

namespace Easteregg\Plugins\Entities;

use Easteregg\Plugins\Exceptions\UnexpectedErrorException;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class PluginInformation
 *
 * @package Easteregg\Plugins\Entities
 */
class PluginInformation implements Arrayable
{
    /**
     * @var
     */
    protected $attributes = [];

    /**
     * PluginInformation constructor.
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        $this->setAttributes($attributes);
    }

    /**
     * @param $key
     *
     * @return null
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * @param $key
     *
     * @return null
     */
    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->getAttributes())) {
            return $this->attributes[$key];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * @param $attributes
     */
    public function setAttributes($attributes)
    {
        if (is_string($attributes) and is_file(realpath($attributes))) {
            $this->attributes = $this->fetchInformation($attributes);
        } else {
            if (! is_array($attributes)) {
                throw new UnexpectedErrorException("Plugin path \"{$attributes}\"is not existed, or the plugin directory structure is invalid.");
            }
            $this->attributes = array_merge($this->attributes, $attributes);
        }

    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * @param $plugin
     *
     * @return \Easteregg\Plugins\Entities\PluginInformation
     */
    private function fetchInformation($plugin)
    {
        $json = json_decode(file_get_contents($plugin), true);
        $path = $this->getParentDirectory($plugin);
        $name = explode("/", $json['name']);
        $autoload = array_keys($json['autoload']['psr-4'])[0];

        return [
            'vendor'      => $name[0], // vendor section of plugin.json name field
            'name'        => $name[1], // name section of plugin.json name field
            'type'        => $json['type'] ?? 'plugin', // defaults to plugin,
            'path'        => $path,
            'version'     => $json['version'] ?? '0.0',
            'autoload'    => $autoload,
            'description' => $json['description'] ?? "",
            'key'         => studly_case($name[0]) . DIRECTORY_SEPARATOR . studly_case($name[1]),
        ];
    }

    /**
     * Direct path to plugin's directory, this will prevent removing or moving unwanted files
     *
     * @param $plugin
     *
     * @return mixed
     */
    private function getParentDirectory($plugin)
    {
        return pathinfo($plugin)['dirname'];
    }

    public function getInstance()
    {
        $load = $this->getAttribute("autoload");
        $plugin = "{$load}Plugin";

        return new $plugin(app());

    }
}
