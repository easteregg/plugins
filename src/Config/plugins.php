<?php

use Easteregg\Plugins\Extraction\Zip;

return [
    'pre' => [
        'path' => env("PLUGINS_PRE_PATH", __DIR__ .'/../storage/app/temp/plugins/'),
        'drivers' => [
            'zip' => Zip::class
        ]
    ],
    'post' => [
        'path' => env('PLUGINS_POST_PATH', __DIR__ .'/../storage/app/plugins/'),

    ],
    'dashboardLayout' => 'plugins::layout'

];
