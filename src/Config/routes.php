<?php

use Easteregg\Plugins\Http\Controller\PluginsController;
use Easteregg\Plugins\Http\Controller\InactivePluginsController;
use Easteregg\Plugins\Http\Controller\InactiveThemesController;
use Easteregg\Plugins\Http\Controller\ActiveThemesController;
use Easteregg\Plugins\Http\Controller\ActivePluginsController;
use Easteregg\Plugins\Http\Controller\ActivatesPluginController;
use Easteregg\Plugins\Http\Controller\DeactivatesPluginController;
use Easteregg\Plugins\Http\Controller\InstallsPluginController;
use Easteregg\Plugins\Http\Controller\PublishesPluginAssetsController;


Route::group(['middleware' => ['web', 'auth', 'acl'], 'prefix' => 'dashboard'], function () {
    Route::group(["prefix" => 'plugins'], function () {
        Route::resource("activate", ActivatesPluginController::class);
        Route::resource("deactivate", DeactivatesPluginController::class);
        Route::resource("inactive-plugins", InactivePluginsController::class);
        Route::resource("active-plugins", ActivePluginsController::class);
        Route::resource("install", InstallsPluginController::class);
        Route::resource("publish", PublishesPluginAssetsController::class);
        Route::resource("inactive-themes", InactiveThemesController::class);
        Route::resource("active-themes", ActiveThemesController::class);
        Route::resource("/", PluginsController::class);
    });
});
