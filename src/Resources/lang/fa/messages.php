<?php

return [
    'plugins'                            => "افزونه‌ها",
    'activePlugins'                      => "افزونه فعال",
    'inactivePlugins'                    => "افزونه غیرفعال",
    'pluginManagement'                   => "مدیریت افزونه",
    'activeThisPlugin'                   => "فعال‌سازی افزونه",
    'deactivateThisPlugin'               => "غیرفعال‌سازی افزونه",
    'manage-plugins'                     => "مدیریت‌ افزونه‌ها",
    'themes'                             => 'قالب‌ها',
    'inactiveThemes'                     => 'قالب غیرفعال',
    'activeThemes'                       => 'قالب فعال',
    'addNewPlugin'                       => 'ایجاد افزونه جدید',
    'upload'                             => 'آپلود',
    'pluginUploadedSuccessfully'         => 'افزونه با موفقیت آپلود شد.',
    'pluginUploadFailedTheFileIsInvalid' => 'آپلود افزونه موفقیت آمیز نبود. فایل افزونه معتبر نیست.',
    'noInactivePluginFound'              => 'هیچ افزونه‌ی غیرفعالی در سیستم وجود ندارد.',
    'noActivePluginFound'                => 'افزونه‌ی فعالی در سیستم وجود ندارد.',
    'pluginName'                         => 'نام افزونه',
    'pluginDescription'                  => 'توضیحات',
    'version'                            => 'نگارش',
    'type'                               => 'نوع',
    'activate'                           => 'فعال‌سازی',
    'deactivate'                         => 'غیرفعال کردن',
    'backToPluginManagement'             => 'بازگشت به مدیریت افزونه‌ها',
    'pluginActivatedSuccessfully'        => 'افزونه با موفقیت فعال شد.',
    'unexpectedErrorHappened'            => "خطای نامشخص رخ داد. در صورت تکرار، با مدیر وب‌سایت تماس بگیرید.",
    'publishPlugin'                      => 'انتشار فایل‌های افزونه',
    'nowPublishPluginAssets'             => 'افزونه با موفقیت فعال شد. اما برای راه‌اندازی کامل، باید اطلاعات آن را هم منتشر کنیم. هر افزونه برای خود به جداول پایگاه داده، تنظیمات و فایل‌های دیگر نیاز دارد. برای انجام این کار به :name بروید و روی گزینه‌ی انتشار کلیک کنید.',
    'publish'                            => 'انتشار',
    'publishedSuccessfully'              => 'فایل‌های پلاگین با موفقیت منتشر شدند.',
    'pluginDeactivatedSuccessfully'      => 'افزونه با موفقیت غیرفعال شد.',
    'noActiveThemesFound'                => 'هیچ قالب فعالی یافت نشد.',
    'noInactiveThemesFound'              => "هیچ قالب غیرفعالی یافت نشد.",

];
