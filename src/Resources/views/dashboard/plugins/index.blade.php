@extends(config("plugins.dashboardLayout"))
@section("pageTitle", trans("plugins::messages.pluginManagement"))

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include("plugins::partials.errors")
                <h1 class="h1">
                    @lang("plugins::messages.pluginManagement")
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="item bg-primary">
                    <div class="item-heading">
                        <h2>@lang("plugins::messages.plugins")</h2>
                    </div>
                    <div class="item-description">
                        <div class="row description-buttons">
                            <div class="col-xs-6">
                                <small>
                                    <a href="{{ url("dashboard/plugins/active-plugins") }}" class="btn btn-default btn-block">
                                        {{ count($activePlugins) }} @lang("plugins::messages.activePlugins")
                                    </a>
                                </small>
                            </div>
                            <div class="col-xs-6">
                                <small>
                                    <a href="{{ url("dashboard/plugins/inactive-plugins") }}" class="btn btn-default btn-block">
                                        {{ count($inactivePlugins) }} @lang("plugins::messages.inactivePlugins")
                                    </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="item bg-primary">
                    <div class="item-heading">
                        <h2>@lang("plugins::messages.themes")</h2>
                    </div>
                    <div class="item-description">
                        <div class="row description-buttons">
                            <div class="col-xs-6">
                                <small>
                                    <a href="{{ url("dashboard/plugins/active-themes") }}" class="btn btn-default btn-block">
                                        {{ count($activeThemes) }} @lang("plugins::messages.activeThemes")
                                    </a>
                                </small>
                            </div>
                            <div class="col-xs-6">
                                <small>
                                    <a href="{{ url("dashboard/plugins/inactive-themes") }}" class="btn btn-default btn-block">
                                        {{ count($inactiveThemes) }} @lang("plugins::messages.inactiveThemes")
                                    </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">@lang("plugins::messages.addNewPlugin")</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url("dashboard/plugins") }}" enctype="multipart/form-data" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="file" name="plugin" class="form-control" accept=".zip" >
                    </div>

                    <button type="submit" class="btn btn-primary">
                        @lang("plugins::messages.upload")
                        <i class="fa fa-upload"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
@stop

@section("dash.head.scripts")
    <style>
        .item {
            display: flex;
            flex-direction:column;
            align-items: center;
            border-radius: 2px;
            padding: 10px;
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }
        .item:hover {
            box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
        }
        .item-description {
            border-top: 1px solid lightgray;
            width: 100%;
        }
        .description-buttons {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            padding-top: 10px;
        }
    </style>
@append
