@extends(config("plugins.dashboardLayout"))
@section("pageTitle", trans("plugins::messages.publishPlugin"))

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include("plugins::partials.errors")
                <h1 class="h1">
                    @lang("plugins::messages.publishPlugin")
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        @lang("plugins::messages.nowPublishPluginAssets", ['name' => '<a href="'.$activeLink.'" class="btn btn-default" >' . trans("plugins::messages.activePlugins") . '</a>'])
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section("dash.head.scripts")
@append
