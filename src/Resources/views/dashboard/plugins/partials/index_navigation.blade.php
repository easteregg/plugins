<div class="row">
    <div class="col-md-12">
        @include("plugins::partials.errors")
        <h1 class="h1">
            @lang("plugins::messages.activePlugins")
            <div class="pull-left">
                <a href="{{ url("dashboard/plugins") }}" class="btn btn-default btn-sm">
                    @lang("plugins::messages.backToPluginManagement")
                </a>
            </div>
        </h1>
    </div>
</div>
