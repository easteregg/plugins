@extends(config("plugins.dashboardLayout"))
@section("pageTitle", trans("plugins::messages.activeThemes"))

@section("content")
    <div class="container">
        @include("plugins::dashboard.plugins.partials.index_navigation")
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed table-hover table-striped">
                        <tr>
                            <th>@lang("plugins::messages.pluginName")</th>
                            <th>@lang("plugins::messages.pluginDescription")</th>
                            <th>@lang("plugins::messages.type")</th>
                            <th>@lang("plugins::messages.version")</th>
                            <th>@lang("plugins::messages.deactivate")</th>
                            <th>@lang("plugins::messages.publish")</th>
                        </tr>
                        @if($activeThemes->count() == 0)
                            <tr>
                                <td colspan="5">
                                    @lang("plugins::messages.noActiveThemesFound")
                                </td>
                            </tr>
                        @endif
                        @foreach($activeThemes as $plugin)
                            <tr>
                                <td>{{ $plugin->getAttribute("vendor") }}/{{ $plugin->getAttribute("name") }}</td>
                                <td>{{ $plugin->getAttribute("description") }}</td>
                                <td>{{ $plugin->getAttribute("type") }}</td>
                                <td>{{ $plugin->getAttribute("version") }}</td>
                                <td>
                                    <form action="{{ url("dashboard/plugins/deactivate") }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="plugin" value="{{ $plugin->getAttribute("key") }}">

                                        <button class="btn btn-danger">
                                            <i class="fa fa-power-off"></i>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ url("dashboard/plugins/publish") }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="plugin" value="{{ $plugin->getAttribute("key") }}">

                                        <button class="btn btn-info" type="submit">
                                            <i class="fa fa-copy"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section("dash.head.scripts")
    <style>
        .item {
            display: flex;
            flex-direction: column;
            align-items: center;
            border-radius: 2px;
            padding: 10px;
            transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        }

        .item:hover {
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
        }

        .item-description {
            border-top: 1px solid lightgray;
            width: 100%;
        }
    </style>
@append
