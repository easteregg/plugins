@extends(config("plugins.dashboardLayout"))
@section("pageTitle", trans("plugins::messages.inactiveThemes"))

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include("plugins::partials.errors")
                <h1 class="h1">
                    @lang("plugins::messages.inactiveThemes")
                    <div class="pull-left">
                        <a href="{{ url("dashboard/plugins") }}" class="btn btn-default btn-sm">
                            @lang("plugins::messages.backToPluginManagement")
                        </a>
                    </div>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-condensed table-hover table-striped">
                        <tr>
                            <th>@lang("plugins::messages.pluginName")</th>
                            <th>@lang("plugins::messages.pluginDescription")</th>
                            <th>@lang("plugins::messages.type")</th>
                            <th>@lang("plugins::messages.version")</th>
                            <th>@lang("plugins::messages.activate")</th>
                        </tr>
                        @if($inactiveThemes->count() == 0)
                            <tr>
                                <td colspan="5">
                                    @lang("plugins::messages.noInactiveThemesFound")
                                </td>
                            </tr>
                        @endif
                        @foreach($inactiveThemes as $themes)
                            <tr>
                                <td>{{ $themes->getAttribute("vendor") }}/{{ $themes->getAttribute("name") }}</td>
                                <td>{{ $themes->getAttribute("description") }}</td>
                                <td>{{ $themes->getAttribute("type") }}</td>
                                <td>{{ $themes->getAttribute("version") }}</td>
                                <td>
                                    <form action="{{ url("dashboard/plugins/activate") }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="plugin" value="{{ $themes->getAttribute("key") }}">

                                        <button class="btn btn-success">
                                            <i class="fa fa-power-off"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section("dash.head.scripts")
    <style>
        .item {
            display: flex;
            flex-direction: column;
            align-items: center;
            border-radius: 2px;
            padding: 10px;
            transition: all 0.3s cubic-bezier(.25, .8, .25, 1);
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
        }

        .item:hover {
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
        }

        .item-description {
            border-top: 1px solid lightgray;
            width: 100%;
        }
    </style>
@append
