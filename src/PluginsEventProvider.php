<?php

namespace Easteregg\Plugins;

use Easteregg\Plugins\Listeners\HandleDashboardMenu;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Easteregg\Haddock\Events\DashboardMenu;

class PluginsEventProvider extends EventServiceProvider
{
    protected $listen = [
        DashboardMenu::class => [
            HandleDashboardMenu::class
        ]
    ];
}
