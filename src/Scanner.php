<?php

namespace Easteregg\Plugins;

use InvalidArgumentException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use RuntimeException;

/**
 * Class Scanner
 *
 * @package Easteregg\Plugins
 */
class Scanner
{
    /**
     * @var
     */
    protected $targetFile;
    /**
     * @var
     */
    protected $sourcePath;

    /**
     * An alias for scan()
     *
     * @return array
     */
    public function get()
    {
        return $this->scan();
    }

    /**
     * Get plugins directory Iterator
     *
     * @return \RegexIterator
     */
    public function getDirectoryIterator(): \RegexIterator
    {
        try {
            return new RegexIterator(
                new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator(realpath($this->getSourcePath()))
                ),
                "/{$this->getTargetFile()}/", RegexIterator::MATCH
            );
        } catch (RuntimeException $exception) {
            throw new \InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function getSourcePath()
    {
        if ($this->sourcePath) {
            return $this->sourcePath;
        }

        return config('plugins.sourcePath', storage_path());

    }

    /**
     * @return string
     */
    public function getTargetFile(): string
    {
        if ($this->targetFile) {
            return $this->targetFile;
        }

        throw new InvalidArgumentException("No target file specified for scanner.");

    }

    /**
     * Set the target path to search.
     *
     * @param $path
     *
     * @return $this
     */
    public function in($path)
    {
        $this->setSourcePath($path);

        return $this;
    }

    /**
     * Set the target file to search for
     *
     * @param $targetFile
     *
     * @return $this
     */
    public function lookFor($targetFile)
    {
        $this->setTargetFile($targetFile);

        return $this;
    }

    /**
     * Scan the $sourcePath to find $targetFile
     *
     * @return array of found [$targetFile]s.
     */
    public function scan(): array
    {
        $iterator = $this->getDirectoryIterator();

        $scannedFiles = [];
        foreach ($iterator as $path => $index) {
            $scannedFiles[] = $path;
        }

        return $scannedFiles;
    }

    /**
     * @param mixed $sourcePath
     */
    public function setSourcePath($sourcePath)
    {
        $this->sourcePath = $sourcePath;
    }

    /**
     * @param mixed $targetFile
     */
    public function setTargetFile($targetFile)
    {
        $this->targetFile = $targetFile;
    }
}
