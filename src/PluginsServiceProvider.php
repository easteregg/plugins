<?php

namespace Easteregg\Plugins;

use Easteregg\Plugins\Contracts\Validator;
use Easteregg\Plugins\Facades\PluginValidator;
use Easteregg\Plugins\Jobs\FetchesPluginInformation;
use Easteregg\Plugins\Validation\Validator as ConcreteValidator;
use Illuminate\Support\ServiceProvider;

/**
 * Class PluginsServiceProvider
 *
 * @package Easteregg\Plugins
 */
class PluginsServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        require __DIR__ . '/Config/routes.php';
        $this->loadViews();
        $this->loadTranslations();
        $this->publishConfig();
        $this->loadActivePlugins();
        $this->registerActivePluginsServiceProvider();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(PluginsEventProvider::class);

        $this->app->bind(Validator::class, function () {
            return new ConcreteValidator();
        });

        $this->app->bind('pluginValidator', Validator::class);

        $this->app->alias('PluginValidator', PluginValidator::class);


        $this->replaceViewFinder();

    }

    private function activePlugins()
    {
        $activePlugins = cache()->get('all-active-plugins');

        $collection = $activePlugins->filter(function ($plugin) {
            return $plugin->getAttribute('type') == 'plugin';
        });

        cache()->forever('active-plugins', $collection);
    }

    private function activeThemes()
    {
        $activeThemes = cache()->get('all-active-plugins');

        $collection = $activeThemes->filter(function ($plugin) {
            return $plugin->getAttribute('type') == 'theme';
        });

        cache()->forever('active-themes', $collection);
    }

    private function allActivePlugins()
    {
        $this->getAllActivePlugins();
    }

    private function allInactivePlugins()
    {
        $collection = $this->getAllInactivePlugins();

        cache()->forever('all-inactive-plugins', $collection);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getAllActivePlugins(): \Illuminate\Support\Collection
    {
        if (! cache()->has('all-active-plugins')) {
            $pathToPlugins = config("plugins.post.path") ?? storage_path("app/plugins/");
            $fetchesPluginInformation = new FetchesPluginInformation($pathToPlugins);
            $collection = $fetchesPluginInformation->handle();
            cache()->forever('all-active-plugins', $collection);
        }

        return cache()->get('all-active-plugins');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getAllInactivePlugins(): \Illuminate\Support\Collection
    {
        if (! cache()->get('all-inactive-plugins')) {
            $pathToInactivePlugins = config("plugins.pre.path") ?? storage_path("app/temp/plugins");
            $fetchesPluginInformation = new FetchesPluginInformation($pathToInactivePlugins);
            cache()->forever('all-inactive-plugins',  $fetchesPluginInformation->handle());
        }

        return cache()->get('all-inactive-plugins');
    }

    private function inactivePlugins()
    {
        $inactivePlugins = cache()->get('all-inactive-plugins');

        $collection = $inactivePlugins->filter(function ($plugin) {
            return $plugin->getAttribute('type') == 'plugin';
        });

        cache()->forever('inactive-plugins', $collection);
    }

    private function inactiveThemes()
    {
        $inactivePlugins = cache()->get('all-inactive-plugins');

        $collection = $inactivePlugins->filter(function ($plugin) {
            return $plugin->getAttribute('type') == 'theme';
        });

        cache()->forever('inactive-themes', $collection);
    }

    /**
     * Register active and inactive plugins. These aliases will be registered in the system:
     *
     * @var Collection all-active-plugins return all active plugins, including themes
     * @var Collection all-inactive-plugins return all inactive plugins, including themes
     * @var Collection active-plugins
     * @var Collection active-themes
     * @var Collection inactive-plugins
     * @var Collection inactive-themes
     */
    private function loadActivePlugins()
    {
        // all active plugins, including themes.
        $this->allActivePlugins();
        $this->allInactivePlugins();
        // Active plugins
        $this->activePlugins();
        // Active themes
        $this->activeThemes();
        // Inactive Plugins
        $this->inactivePlugins();
        // Inactive Themes
        $this->inactiveThemes();

        $this->app->bind('all-active-plugins', cache()->get('all-active-plugins'));
        $this->app->bind('all-inactive-plugins', cache()->get('all-inactive-plugins'));
        $this->app->bind('active-plugins', cache()->get('active-plugins'));
        $this->app->bind('active-themes', cache()->get('active-themes'));
        $this->app->bind('inactive-plugins', cache()->get('inactive-plugins'));
        $this->app->bind('inactive-themes', cache()->get('inactive-themes'));
    }

    /**
     *
     */
    private function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang', 'plugins');
    }

    /**
     *
     */
    private function loadViews()
    {
        $this->loadViewsFrom(__DIR__ . '/Resources/views', 'plugins');
    }

    /**
     *
     */
    private function publishConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/Config/plugins.php', 'plugins');
        $this->publishes([
            __DIR__ . '/Config/plugins.php' => config_path('plugins.php'),
        ], 'plugins.config');
    }

    private function registerActivePluginsServiceProvider()
    {
        $activePlugins = cache()->get('all-active-plugins');
        if ($this->app->environment() !== "testing") {
            foreach ($activePlugins as $plugin) {
                $this->app->register($plugin->getAttribute('autoload') . 'Plugin');
            }
        }
    }

    private function replaceViewFinder()
    {
        $oldViewFinder = $this->app['view.finder'];

        $this->app->bind('view.finder', function($app) use ($oldViewFinder) {
            return new ThemeFileViewFinder(
                $oldViewFinder->getFileSystem(),
                $oldViewFinder->getPaths(),
                $oldViewFinder->getExtensions()
            );
        });
    }
}
