<?php

namespace Easteregg\Plugins;

use Easteregg\Plugins\Contracts\PluginContract;
use Illuminate\Support\ServiceProvider;

abstract class PluginBaseServiceProvider extends ServiceProvider implements PluginContract
{
    public function boot()
    {
        $this->bootPluginServiceProviders();
    }

    private function bootPluginServiceProviders()
    {
        $providers = $this->serviceProviders();
        foreach ($providers as $provider) {
            if (class_exists($provider)) {
                $this->app->register($provider);
            }
        }
    }

    /**
     * @return array
     */
    public function views(): array
    {
        return [];
    }

}
