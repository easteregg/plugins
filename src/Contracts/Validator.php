<?php

namespace Easteregg\Plugins\Contracts;

/**
 * Interface Validator
 *
 * @package Easteregg\Plugins\Contracts
 */
interface Validator
{
    /**
     * Make an instance of the validator
     *
     * @param string $path
     *
     * @return \Easteregg\Plugins\Contracts\Validator
     */
    public static function make(string $path) : self;

    /**
     * Check if the package passes the validation.
     *
     * @return bool
     */
    public function passes(): bool;

    /**
     * Check if the given package fails to validate.
     *
     * @return bool
     */
    public function fails() : bool;
}
