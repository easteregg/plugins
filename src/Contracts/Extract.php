<?php

namespace Easteregg\Plugins\Contracts;

/**
 * Interface Extract
 *
 * @package Easteregg\Plugins\Contracts
 */
interface Extract
{
    /**
     * @return string
     */
    public function extract() : string;
}
