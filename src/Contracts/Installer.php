<?php

/**
 * Interface Decompressor
 */
interface Decompressor
{
    /**
     * Decompress a compressed plugin into default path
     *
     * @return void
     */
    public function extract() : void;
}
