<?php

namespace Easteregg\Plugins\Contracts;

/**
 * Interface PluginContract
 *
 * @package Easteregg\Plugins\Contracts
 */
/**
 * Interface PluginContract
 *
 * @package Easteregg\Plugins\Contracts
 */
interface PluginContract
{
    /**
     * Actions needed to take place when trying to activate a method
     *
     * @return void
     */
    public function activate();

    /**
     * Actions needed to take place when trying to deactivate a method
     *
     * @return void
     */
    public function deactivate();

    /**
     * Get the list of plugin service providers
     *
     * @return array
     */
    public function serviceProviders(): array;

    /**
     * @return array
     */
    public function views(): array;
}
