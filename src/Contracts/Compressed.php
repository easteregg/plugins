<?php

namespace Easteregg\Plugins\Contracts;

/**
 * Interface Compressed
 *
 * @package Easteregg\Plugins\Contracts
 */
/**
 * Interface Compressed
 *
 * @package Easteregg\Plugins\Contracts
 */
interface Compressed
{
    /**
     * Get filename
     *
     * @return string
     */
    public function getFileName(): string;

    /**
     * Get the current path to the compressed file.
     *
     * @return string
     */
    public function getPath(): string;

    /**
     * set the destination to extract the plugin
     *
     * @param string $path
     *
     * @return \Easteregg\Plugins\Contracts\Compressed
     */
    public function to(string $path);

    /**
     * Get the destination
     *
     * @return string
     */
    public function getDestination(): string;

    /**
     * extract the compressed file to the specified path
     *
     * @return bool
     */
    public function extract(): bool;

    /**
     * Get directory name of the extracted folder.
     *
     * @return string|array
     */
    public function getExtractedDirectory();
}
