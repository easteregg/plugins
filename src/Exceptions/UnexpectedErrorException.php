<?php

namespace Easteregg\Plugins\Exceptions;

use RuntimeException;

class UnexpectedErrorException extends RuntimeException
{

}
