<?php

namespace Easteregg\Plugins\Listeners;
use Easteregg\Haddock\HaddockContract;


class HandleDashboardMenu
{
    protected $menu;

    public function __construct(HaddockContract $menu)
    {
        $this->menu = $menu;

        $this->menu->title = trans("plugins::messages.manage-plugins");
        $this->menu->icon = "fa fa-power-off";
//        $this->menu->url = url('dashboard/plugins');
        $this->menu->addChildren([
            [
                "title" => trans("plugins::messages.manage-plugins"),
                "icon" => "fa fa-power-off",
                "url" => url('dashboard/plugins')
            ],
            [
                "title" => trans("plugins::messages.activePlugins"),
                "icon" => "fa fa-power-off",
                "url" => url("dashboard/plugins/active-plugins")
            ],
            [
                "title" => trans("plugins::messages.activeThemes"),
                "icon" => "fa fa-power-off",
                "url" => url("dashboard/plugins/active-themes")
            ]
        ]);
    }

    public function handle()
    {
        return $this->menu;
    }
}
