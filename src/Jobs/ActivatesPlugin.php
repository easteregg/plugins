<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Entities\PluginInformation;
use Easteregg\Plugins\Exceptions\UnexpectedErrorException;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ActivatesPlugin
 * This Job will activate the plugin using a path to plugin.json.
 * Activation process consists of the following:
 * Get the parent working directory of plugin.json
 * move the plugin directory to destination path (destination path is defined in the config, also it should be equal
 * to what is defined in `wikimedia/composer-merge-plugin`'s extra field). It will dump autoload to register the
 * plugins autoload namespace in the destination directory.
 * From this step, It will autoload Plugin.php file (which extends PluginBaseServiceProvider).
 * Then it will call the activate method on Plugin.php file, which is outside of this class' responsibility, but
 * it will migrate and publish and do stuff that is necessary.
 *
 * @see     \Easteregg\Plugins\PluginBaseServiceProvider
 * @package Easteregg\Plugins\Jobs
 */
class ActivatesPlugin extends Job implements ShouldQueue
{
    /**
     * @var
     */
    protected $pluginJsonPath;

    /**
     * @var PluginInformation
     */
    protected $pluginInformation;

    /**
     * ActivatesPlugin constructor.
     *
     * @param $pluginJsonPath
     */
    public function __construct($pluginJsonPath)
    {
        $this->setPluginJsonPath($pluginJsonPath);
        $this->setPluginInformation();
    }

    /**
     * Get the destination path to the plugin.
     *
     * @return string
     */
    public function getDestinationPath(): string
    {
        return config("plugins.post.path", storage_path("app/plugins/"));
    }

    /**
     * @return PluginInformation
     */
    public function getPluginInformation(): PluginInformation
    {
        return $this->pluginInformation;
    }

    /**
     * @return string
     */
    public function getPluginJsonPath(): string
    {
        return $this->pluginJsonPath;
    }

    /**
     *
     */
    public function handle()
    {
        $vendor = studly_case($this->getPluginInformation()->getAttribute('vendor'));
        $name = studly_case($this->getPluginInformation()->getAttribute('name'));
        $destination = $this->getDestinationPath();
        $parentDirectory = $this->getPluginInformation()->getAttribute('path');

        $this->assertDestinationPathExists($vendor);
        $this->assertDestinationPluginIsRemovedFirst($vendor, $name);
        $destinationPath = $this->movePluginToDestination($parentDirectory, $destination, $vendor, $name);
        dispatch(new DumpsAutoload());

        return $destinationPath;
    }

    /**
     * @param $pluginJsonPath
     */
    public function setPluginJsonPath($pluginJsonPath)
    {
        $this->pluginJsonPath = $pluginJsonPath;
    }

    /**
     * Create a new Plugin information file.
     */
    protected function setPluginInformation()
    {
        $this->pluginInformation = new PluginInformation($this->pluginJsonPath);
    }

    /**
     * @param $vendor
     */
    private function assertDestinationPathExists($vendor)
    {
        if (! is_dir($this->getDestinationPath())) {
            mkdir($this->getDestinationPath(), 0775, true);
        }

        if (! is_dir($this->getDestinationPath() . DIRECTORY_SEPARATOR . $vendor)) {
            mkdir($this->getDestinationPath() . DIRECTORY_SEPARATOR . $vendor);
        }
    }

    /**
     * Assert that previously installed plugin with the same name is removed.
     *
     * @param $vendor
     * @param $name
     */
    private function assertDestinationPluginIsRemovedFirst($vendor, $name)
    {
        $path = $this->getDestinationPath() . $vendor . DIRECTORY_SEPARATOR . $name;
        if ($res = is_dir($path)) {

            exec("rm $path -rf");
        }
    }

    /**
     * Move plugin to destination
     *
     * @param $parentDirectory
     * @param $destination
     * @param $vendor
     * @param $name
     *
     * @return string new plugin path
     */
    private function movePluginToDestination($parentDirectory, $destination, $vendor, $name): string
    {
        if (! rename($parentDirectory, $destination . $vendor . DIRECTORY_SEPARATOR . $name)) {
            throw new UnexpectedErrorException("Activating Plugin Failed while moving plugin to new directory.");
        }

        return $destination . $vendor . DIRECTORY_SEPARATOR . $name;
    }

}
