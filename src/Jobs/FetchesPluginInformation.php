<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Entities\PluginInformation;
use Easteregg\Plugins\Exceptions\InvalidFileException;
use Easteregg\Plugins\Scanner;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

/**
 * Class FetchesPluginInformation
 *
 * @package Easteregg\Plugins\Jobs
 */
class FetchesPluginInformation extends Job implements ShouldQueue
{
    /**
     * @var string
     */
    protected $path;
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $plugins;

    /**
     * FetchesPluginInformation constructor.
     *
     * @param string $pluginPath
     */
    public function __construct(string $pluginPath)
    {
        $this->setPath($pluginPath);
        $this->plugins = new Collection();
    }

    /**
     * @return array
     */
    public function findPluginJson()
    {
        $scanner = new Scanner();
        try {
            $files = $scanner->lookFor("plugin.json")
                ->in($this->getPath())
                ->get();
            return $files;
        } catch (\InvalidArgumentException $exception) {
            return [];
        }

    }

    /**
     * Get the path to the directory
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Fetches all the plugins in the given path.
     *
     * @return Collection
     */
    public function handle()
    {
        $plugins = $this->findPluginJson();

        foreach ($plugins as $plugin) {
            $this->plugins->push(new PluginInformation($plugin));
        }

        return $this->plugins;
    }

    /**
     * @param string $pluginPath
     */
    public function setPath(string $pluginPath)
    {
        $this->path = $pluginPath;
    }

}
