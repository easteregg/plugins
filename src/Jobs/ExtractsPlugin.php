<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Extraction\Factory;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Class ExtractsPlugin
 *
 * @package Easteregg\Plugins\Jobs
 */
class ExtractsPlugin extends Job
{
    /**
     * @var
     */
    protected $plugin;

    /**
     * @var mixed
     */
    protected $drivers;

    /**
     * ExtractsPlugin constructor.
     *
     * @param $path
     */
    public function __construct(string $path)
    {
        $this->setPlugin($path);
    }


    /**
     * @return string
     */
    public function getPlugin()
    {
        return $this->plugin;
    }

    /**
     * Handles the Extraction of plugin.
     */
    public function handle()
    {
        $this->assertFileExists();

        if ($this->getPlugin()->to($this->getDestinationPath())->extract()) {
            $this->getDestinationPath();

            $extracted = $this->getPlugin()->getExtractedDirectory();
            $pluginInformation = (new FetchesPluginInformation($extracted))->handle();
            $jsonDirectory = $pluginInformation->first()->getAttribute('path');
            $destinationPath = config('plugins.pre.path') . DIRECTORY_SEPARATOR . $pluginInformation->first()->getAttribute('key');
            if (! is_dir($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }

            rename($jsonDirectory, $destinationPath);

            return $destinationPath;

        }

        return null;
    }


    /**
     * @param string $plugin
     */
    public function setPlugin($plugin)
    {
        $this->plugin = Factory::make($plugin);
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function assertFileExists()
    {
        if (! $path = realpath($this->getPlugin()->getPath())) {
            throw new FileNotFoundException();
        }

        return $path;
    }

    private function getDestinationPath()
    {
        return config('plugins.pre.path', storage_path('app/temp/plugins'));
    }
}
