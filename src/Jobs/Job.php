<?php

namespace Easteregg\Plugins\Jobs;

use Illuminate\Bus\Queueable;

abstract class Job
{
    use Queueable;
}
