<?php

namespace Easteregg\Plugins\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadsPlugin extends Job implements ShouldQueue
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    protected $file;
    /**
     * @var string
     */
    protected $targetDirectory;

    /**
     * @var string
     */
    protected $moved;

    /**
     * UploadsPlugin constructor.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
        $this->targetDirectory = config("plugins.pre.path", storage_path("app/temp/plugins"));
    }

    /**
     * Get the uploaded file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile(): \Symfony\Component\HttpFoundation\File\UploadedFile
    {
        return $this->file;
    }

    /**
     * Get target directory
     *
     * @return mixed
     */
    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    /**
     * Handles the job
     *
     * @return string path to the moved file
     */
    public function handle(): string
    {
        $this->assertTargetDirectoryExists();

        $this->moveUploadedFileToTargetPath();

        return $this->moved;
    }

    /**
     * file setter
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return void
     */
    public function setFile(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Set the target directory for plugin to get uploaded.
     *
     * @param mixed $targetDirectory
     *
     * @return void
     */
    public function setTargetDirectory($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * Asserts that Target Directory exists.
     *
     * @return void
     */
    private function assertTargetDirectoryExists()
    {
        File::makeDirectory($this->getTargetDirectory(), 0755, true, true);
    }

    /**
     * Move uploaded file to target path
     *
     * @return void
     */
    private function moveUploadedFileToTargetPath()
    {
        $targetFile = uniqid();
        $extension = $this->getFile()->guessExtension();
        $targetFile .= ".$extension";
        $this->getFile()->move($this->getTargetDirectory(), $targetFile);

        $this->moved = realpath($this->getTargetDirectory() . DIRECTORY_SEPARATOR . $targetFile);
    }
}
