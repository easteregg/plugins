<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Contracts\PluginContract;
use Easteregg\Plugins\Entities\PluginInformation;
use Easteregg\Plugins\Exceptions\UnexpectedErrorException;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class DeactivatesPlugin
 *
 * @package Easteregg\Plugins\Jobs
 */
class DeactivatesPlugin implements ShouldQueue
{
    /**
     * @var \Easteregg\Plugins\Entities\PluginInformation
     */
    protected $plugin;

    /**
     * DeactivatesPlugin constructor.
     *
     * @param string $pluginPath
     */
    public function __construct(string $pluginPath)
    {
        $this->plugin = new PluginInformation($pluginPath);
    }

    /**
     * @return \Easteregg\Plugins\Entities\PluginInformation
     */
    public function getPlugin(): \Easteregg\Plugins\Entities\PluginInformation
    {
        return $this->plugin;
    }


    /**
     * Deactivates a plugin, then moves the plugin to its temp directory
     */
    public function handle()
    {
        $this->assertTempDirectoryExists();
        $plugin = $this->getPluginObject();
        $result = $plugin->deactivate();

        // If somehow the plugin failed to move itself, we bail.
        try {
            $this->movePluginToTemp();
        } catch (\Exception $e) {
            throw new UnexpectedErrorException("Could not deactivate plugin while moving plugin directory out of path. the message was {$e->getMessage()}");
        }

        // dump autoloads to remove plugin from running paths
        dispatch(new DumpsAutoload());

        return $result;
    }

    /**
     * Set plugin information
     *
     * @param \Easteregg\Plugins\Entities\PluginInformation $plugin
     */
    public function setPlugin(\Easteregg\Plugins\Entities\PluginInformation $plugin)
    {
        $this->plugin = $plugin;
    }

    /**
     *
     */
    private function assertTempDirectoryExists()
    {
        if (! is_dir(config("plugins.pre.path"))) {
            mkdir(config("plugins.pre.path"), 0755, true);
        }
    }

    /**
     * Get plugin object
     *
     * @return PluginContract
     */
    private function getPluginObject()
    {
        $plugin = $this->getPlugin()->getAttribute('autoload') . 'Plugin';

        return new $plugin(app());
    }

    /**
     * @return bool
     */
    private function movePluginToTemp()
    {
        $src = $this->getPlugin()->path;
        $dest = config("plugins.pre.path") .  $this->getPlugin()->getAttribute('key');
        $vendorDist = studly_case($this->getPlugin()->getAttribute('vendor'));
        if (! is_dir(config('plugins.pre.path') . $vendorDist)) {
            mkdir(config('plugins.pre.path') . $vendorDist);
        }

        return rename($src, $dest);
    }

}
