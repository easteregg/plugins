<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Facades\PluginValidator;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ValidatesPlugin
 *
 * @package Easteregg\Plugins\Jobs
 */
class ValidatesPlugin extends Job implements ShouldQueue
{
    /**
     * @var
     */
    protected $path;

    /**
     * ValidatesPlugin constructor.
     *
     * @param $path
     */
    public function __construct($path)
    {
        $this->setPath($path);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Validates Plugin
     */
    public function handle()
    {
        if (PluginValidator::make($this->getPath())->passes()) {
            // Copy the folder structure to destination
            return true;
        }

        // Remove the bad folder structure.
        $this->rrmdir($this->getPath());

        return false;
    }

    /**
     * Set the path
     *
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    protected function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
