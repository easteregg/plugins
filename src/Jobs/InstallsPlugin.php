<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Entities\PluginInformation;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class InstallsPlugin
 *
 * @package Easteregg\Plugins\Jobs
 */
class InstallsPlugin implements ShouldQueue
{
    /**
     * @var
     */
    protected $plugin;

    /**
     * InstallsPlugin constructor.
     *
     * @param $pluginJson
     */
    public function __construct($pluginJson)
    {
        $this->setPlugin($pluginJson);
    }

    /**
     *
     */
    public function handle()
    {
        $plugin = $this->bootPlugin();

        return $plugin->activate();
    }

    private function bootPlugin()
    {
        $plugin = $this->plugin->getAttribute('autoload') . 'Plugin';

        return new $plugin(app());
    }

    private function setPlugin($pluginJson)
    {
        $this->plugin = new PluginInformation($pluginJson);
    }
}
