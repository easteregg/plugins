<?php

namespace Easteregg\Plugins\Jobs;

use Easteregg\Plugins\Exceptions\UnexpectedErrorException;
use Illuminate\Support\Composer;
use Symfony\Component\Process\Process;

class DumpsAutoload
{
    /**
     *
     */
    public function handle()
    {
//        if (app()->environment() == "testing") {
//            $process = new Process('', __DIR__ .'/../../');
//            $process->setCommandLine('php composer.phar dumpautoload --optimize');
//            $res = $process->run();
//            if ($res == 1) {
//                throw new UnexpectedErrorException("Activating Plugin namespace failed while dumping autoload.");
//            }
//        } else {
            $composer = app()->make(Composer::class);
            $composer->setWorkingPath(base_path());
            $composer->dumpOptimized();
//        }
    }
}
