<?php

namespace Easteregg\Plugins;


use Easteregg\ThemeManager\Services\ThemeResolver;
use Illuminate\View\FileViewFinder;
use Illuminate\View\ViewFinderInterface;

class ThemeFileViewFinder extends FileViewFinder implements ViewFinderInterface
{
    /**
     * Get the path to a template with a named path.
     *
     * @param  string  $name
     * @return string
     */
    protected function findNamespacedView($name)
    {
        list($namespace, $view) = $this->parseNamespaceSegments($name);

        $themes = cache()->get("active-themes");


        $themes->each(function ($theme) use ($namespace) {
            if (class_exists($theme->getAttribute("autoload") . "Plugin")) {
                $instance = $theme->getInstance();
            } else {
                return;
            }


            $viewPaths = $instance->views();

            foreach ($viewPaths as $path) {
                array_unshift($this->hints[$namespace], $path);
            }
        });

        return $this->findInPaths($view, $this->hints[$namespace]);
    }

}
