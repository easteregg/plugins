<?php

namespace Easteregg\Plugins\Validation;

use Easteregg\Plugins\Contracts\Validator as ValidatorContract;
use Easteregg\Plugins\Scanner;

/**
 * Class Validator
 *
 * @package Easteregg\Plugins\Validation\Validator
 */
class Validator implements ValidatorContract
{
    /**
     * @var
     */
    protected $path;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Check if the given package fails to validate.
     *
     * @return bool
     */
    public function fails(): bool
    {
        return ! $this->passes();
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        return null;
    }

    /**
     * Make an instance of the validator
     *
     * @param string $path
     *
     * @return \Easteregg\Plugins\Contracts\Validator
     */
    public static function make(string $path): ValidatorContract
    {
        $self = new static();

        $self->setAttribute('src', $path);

        return $self;
    }

    /**
     * Check if the package passes the validation.
     *
     * @return bool
     */
    public function passes(): bool
    {
        $source = $this->getAttribute('src');

        if (
            $this->pathExists($source)
            and $this->isDirectory($source)
            and $this->containsPluginJson($source)
            and $this->containsPluginProvider($source)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * @param $source
     *
     * @return bool
     */
    protected function pathExists($source): bool
    {
        if ($source and realpath($source)) {
            return true;
        }

        return false;
    }

    private function containsPluginJson($source)
    {
        $scanner = new Scanner();
        return !! $scanner->lookFor('plugin.json')
            ->in($source)
            ->get();
    }

    private function containsPluginProvider($source)
    {
        $scanner = new Scanner();
        return !! $scanner->lookFor('Plugin.php')
            ->in($source)
            ->get();
    }


    /**
     * @param $source
     *
     * @return bool
     */
    private function isDirectory($source)
    {
        return is_dir($source);
    }
}
