<?php

namespace Easteregg\Plugins\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PluginValidator
 *
 * @package Easteregg\Plugins\Facades
 * @see     Easteregg\Plugins\Validation\Validator
 * @method passes
 * @method fails
 * @method Easteregg\Plugins\Validation\Validator::make
 */
class PluginValidator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pluginValidator';
    }
}
