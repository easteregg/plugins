<?php

namespace Easteregg\Plugins\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class PluginRequest extends FormRequest
{
    public function authorize()
    {
       return true;
    }

    public function rules()
    {
        return [
            'plugin' => ['required', 'file','mimes:zip']
        ];
    }
}
