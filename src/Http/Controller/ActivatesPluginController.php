<?php

namespace Easteregg\Plugins\Http\Controller;

use Easteregg\Plugins\Jobs\ActivatesPlugin;
use Easteregg\Plugins\Jobs\DumpsAutoload;
use Easteregg\Plugins\Scanner;
use Easteregg\Plugins\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ActivatesPluginController extends Controller
{
    public function store(Request $request)
    {
        $validation = Validator::make(config("plugins.pre.path") . $request->plugin);
        $pathToJson = config("plugins.pre.path") . $request->plugin . "/plugin.json";
        if ($validation->passes()) {
            $path = (new ActivatesPlugin($pathToJson))->handle();
            (new DumpsAutoload())->handle();
            session()->flash("message", trans("plugins::messages.pluginActivatedSuccessfully"));
            return redirect("dashboard/plugins/install");
        }

        session()->flash("message", trans("plugins::messages.unexpectedErrorHappened"));

        return redirect("dashboard/plugins/inactive-plugins");
    }


}
