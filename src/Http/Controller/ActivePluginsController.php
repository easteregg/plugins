<?php

namespace Easteregg\Plugins\Http\Controller;

use Illuminate\Routing\Controller;

class ActivePluginsController extends Controller
{
    public function index()
    {
        $activePlugins = cache()->get("active-plugins");

        return view("plugins::dashboard.plugins.active.index", compact("activePlugins"));
    }
}
