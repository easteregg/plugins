<?php

namespace Easteregg\Plugins\Http\Controller;

use Easteregg\Plugins\Jobs\DeactivatesPlugin;
use Easteregg\Plugins\Jobs\DumpsAutoload;
use Easteregg\Plugins\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class DeactivatesPluginController extends Controller
{
    public function store(Request $request)
    {
        $validation = Validator::make(config("plugins.post.path") . $request->plugin);
        $pathToJson = config("plugins.post.path") . $request->plugin . "/plugin.json";
        if ($validation->passes()) {
            try {
                (new DeactivatesPlugin($pathToJson))->handle();
                session()->flash("message", trans("plugins::messages.pluginDeactivatedSuccessfully"));
                (new DumpsAutoload())->handle();
                return redirect("dashboard/plugins");
            } catch (UnexpectedErrorException $e) {
                Log::debug($e);
            } catch (\ErrorException $e) {
                Log::debug($e);
            }
        }
        Log::debug($pathToJson);

        session()->flash("message", trans("plugins::messages.unexpectedErrorHappened"));

        return redirect("dashboard/plugins/active-plugins");
    }
}
