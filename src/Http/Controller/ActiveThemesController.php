<?php

namespace Easteregg\Plugins\Http\Controller;

use Illuminate\Routing\Controller;

class ActiveThemesController extends Controller
{
    public function index()
    {
        $activeThemes = cache()->get('active-themes');

        return view("plugins::dashboard.themes.active.index",
            compact("activeThemes"));
    }
}
