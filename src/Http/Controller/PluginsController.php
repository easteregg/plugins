<?php

namespace Easteregg\Plugins\Http\Controller;

use Easteregg\Plugins\Entities\PluginInformation;
use Easteregg\Plugins\Http\Request\PluginRequest;
use Easteregg\Plugins\Jobs\ExtractsPlugin;
use Easteregg\Plugins\Jobs\FetchesPluginInformation;
use Easteregg\Plugins\Jobs\UploadsPlugin;
use Easteregg\Plugins\Scanner;
use Easteregg\Plugins\Validation\Validator;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Routing\Controller;

class PluginsController extends Controller
{
    public function index()
    {
        $activePlugins = cache()->get('active-plugins');
        $inactivePlugins = cache()->get('inactive-plugins');
        $activeThemes = cache()->get("active-themes");
        $inactiveThemes = cache()->get("inactive-themes");

        return view("plugins::dashboard.plugins.index",
            compact("activePlugins", "inactivePlugins", 'activeThemes', 'inactiveThemes'));
    }

    public function store(PluginRequest $request)
    {
        try {
            $movedFilePath = (new UploadsPlugin($request->plugin))->handle();
            (new ExtractsPlugin($movedFilePath))->handle();
//            $pluginInformation = new PluginInformation($extracted . "plugin.json");
//            mkdir(storage_path('app/temp/plugins/' . $pluginInformation->getAttribute('key')), 0755, true);
//            rename($extracted, storage_path('app/temp/plugins/' . $pluginInformation->getAttribute('key')));
            session()->flash("message", trans("plugins::messages.pluginUploadedSuccessfully"));
            return redirect("dashboard/plugins");
        } catch (FileNotFoundException $exception) {
            session()->flash("message", trans("plugins::messages.pluginUploadFailedTheFileIsInvalid"));
            return redirect("dashboard/plugins");
        }
    }
}
