<?php

namespace Easteregg\Plugins\Http\Controller;

use Easteregg\Plugins\Http\Request\PluginRequest;
use Easteregg\Plugins\Jobs\ExtractsPlugin;
use Easteregg\Plugins\Jobs\UploadsPlugin;
use Easteregg\Plugins\Validation\Validator;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Routing\Controller;

class InactivePluginsController extends Controller
{
    public function index()
    {
        $inactivePlugins = cache()->get('inactive-plugins');

        return view("plugins::dashboard.plugins.inactive.index",
            compact("inactivePlugins"));
    }
}
