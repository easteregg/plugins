<?php

namespace Easteregg\Plugins\Http\Controller;

use Illuminate\Routing\Controller;

class InstallsPluginController extends Controller
{
    public function index()
    {
        if (url()->previous() == url("dashboard/plugins/inactive-themes")) {
            $activeLink = url("dashboard/plugins/active-themes");
        } else {
            $activeLink = url("dashboard/plugins/active-plugins");
        }
        return view("plugins::dashboard.plugins.install.index", compact('activeLink'));
    }
}
