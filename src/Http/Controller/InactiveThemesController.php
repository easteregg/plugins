<?php

namespace Easteregg\Plugins\Http\Controller;

use Illuminate\Routing\Controller;

class InactiveThemesController extends Controller
{
    public function index()
    {
        $inactiveThemes = cache()->get('inactive-themes');

        return view("plugins::dashboard.themes.inactive.index",
            compact("inactiveThemes"));
    }
}
