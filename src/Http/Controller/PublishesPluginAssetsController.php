<?php

namespace Easteregg\Plugins\Http\Controller;

use Easteregg\Plugins\Jobs\InstallsPlugin;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PublishesPluginAssetsController extends Controller
{
    public function store(Request $request)
    {
        $plugin = config("plugins.post.path") . $request->plugin . DIRECTORY_SEPARATOR . "plugin.json";
        dispatch(new InstallsPlugin($plugin));

        session()->flash('message', trans("plugins::messages.publishedSuccessfully"));
        if (url('plugins/active-plugins') == url()->previous()) {
            return redirect('dashboard/plugins/active-plugins');
        }

        return redirect('dashboard/plugins/active-themes');

    }
}
