<?php

namespace Easteregg\Plugins\Extraction;

use Easteregg\Plugins\Contracts\Compressed;
use ZipArchive;

class Zip implements Compressed
{
    protected $file;
    protected $targetPath;
    protected $extracted;
    /**
     * @var \ZipArchive
     */
    protected $zipArchive;

    public function __construct($compressedFile, $zipArchive = null)
    {
        $this->setFile($compressedFile);

        if ($zipArchive instanceof ZipArchive) {
            $this->setZipArchive($zipArchive);
        }
    }

    /**
     * extract the compressed file to the specified path
     *
     * @return bool
     */
    public function extract(): bool
    {
        $zip = $this->getZipArchive();
        $result = $zip->open($this->getFile(), ZipArchive::CHECKCONS);
        if ($result !== true) {
            $this->removeZipFile();
            return false;
        }

        try {
            $zip->extractTo($this->getDestination());
            for ($i = 0; $i < 100; $i++) {

                if (! starts_with($zip->getNameIndex($i), ".")) {
                    $this->makeExtractedPath($zip->getNameIndex(0));
                    break;
                }
            }

            $zip->close();
            $this->removeZipFile();
            return true;
        } catch (\Exception $exception) {
            $zip->close();
            $this->removeZipFile();
            return false;
        }
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFileName(): string
    {
        $fileName = pathinfo($this->getFile())['basename'];

        return explode(".", $fileName)[0];
    }

    /**
     * Get the current path to the compressed file.
     *
     * @return string
     */
    public function getPath(): string
    {
        return pathinfo($this->getFile())['dirname'];
    }

    /**
     * @param \ZipArchive $zipArchive
     */
    public function setZipArchive(\ZipArchive $zipArchive)
    {
        $this->zipArchive = $zipArchive;
    }

    /**
     * @return mixed
     */
    protected function getTargetPath()
    {
        return $this->targetPath;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @param mixed $targetPath
     */
    public function setTargetPath($targetPath)
    {
        $this->targetPath = $targetPath;
    }

    /**
     * set the destination to extract the plugin
     *
     * @param string $path
     *
     * @return \Easteregg\Plugins\Contracts\Compressed
     */
    public function to(string $path)
    {
        $this->setTargetPath($path);

        return $this;
    }

    /**
     * Get the destination
     *
     * @return string
     */
    public function getDestination(): string
    {
        return $this->getTargetPath();
    }

    /**
     * Get directory name of the extracted folder.
     *
     * @return string|array
     */
    public function getExtractedDirectory()
    {
        return $this->extracted;
    }

    /**
     * @return \ZipArchive
     */
    public function getZipArchive(): \ZipArchive
    {
        if ($this->zipArchive instanceof ZipArchive) {
            return $this->zipArchive;
        }

        return new ZipArchive();
    }

    private function makeExtractedPath($extractedDirectory)
    {
        $destination = rtrim($this->getDestination(), "/");
        $this->extracted = $destination . DIRECTORY_SEPARATOR . $extractedDirectory;
    }

    private function removeZipFile()
    {
        @unlink($this->getFile());
    }
}
