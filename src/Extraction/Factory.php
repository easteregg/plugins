<?php

namespace Easteregg\Plugins\Extraction;

use Easteregg\Plugins\Exceptions\InvalidFileException;

/**
 * Class Factory
 *
 * @package Easteregg\Plugins\Extraction
 */
class Factory
{
    /**
     * @var array
     */
    protected static $drivers;

    /**
     * Factory constructor.
     */
    public function __construct()
    {
        throw new \InvalidArgumentException("Easteregg\\Plugins\\Extraction\\Factory should not be instantiated directly. use static::make() method.");
    }

    /**
     * Get driver class based on key
     *
     * @param $key
     *
     * @return string|null
     */
    public static function getDriver($key)
    {
        if (array_key_exists($key, static::getDrivers())) {
            return static::getDrivers()[$key];
        }

        return null;
    }

    /**
     * Get drivers.
     *
     * @return array
     */
    public static function getDrivers(): array
    {
        return static::$drivers;
    }

    /**
     * Get extension of the given path to file
     *
     * @param $compressed
     *
     * @return mixed
     */
    public static function getExtension($compressed)
    {
        return pathinfo($compressed)['extension'];
    }

    /**
     * Make extraction driver for the given file
     *
     * @param $compressed
     *
     * @return static
     * @throws \Easteregg\Plugins\Exceptions\InvalidFileException
     */
    public static function make($compressed)
    {
        static::boot();

        $key = static::getExtension($compressed);
        if ($driver = static::getDriver($key)) {

            return new $driver($compressed);
        }

        throw new InvalidFileException("Invalid File provided for plugin.");
    }

    /**
     * Set drivers.
     *
     * @param array $drivers
     */
    public static function setDrivers(array $drivers)
    {
        static::$drivers = $drivers;
    }

    protected static function boot()
    {
        static::setDrivers(config('plugins.pre.drivers', []));
    }

}
