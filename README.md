# Laravel 5 Plugin system

## Introduction

This package provides a hot-plugin-installer/loader with composer autoloading. 

## Getting Started

This package consists of two internal commands

1. Uploader
2. Installer

### Uploader 
This Command will upload a compressed file into given directory, preferably a
storage directory (to prevent running the command), and then checks if the uploaded
compressed file is of type (plugin). 

If there is a plugin uploaded, it fetches the metadata from the plugin.json file, and return it as a 
Plugin object.

If there is no plugin uploaded, it removes the given uploaded directory and returns with an error. 

### Installer
 
Installer package works on the Plugin object, and runs the following commands in order: 

1. run php artisan optimize to register the plugins namespace into system.
2. add the plugin information to database.


## Plugin Object

Each plugin object have these methods implemented, so the system will know when to work with it: 

1. The activation/deactivation methods: 

        Note:Each plugin will describe the process of activation and deactivation in a series of artisan calls. 
2. migratedb/rollbackdb methods: 

        Note:Each plugin will describe the process of database migrations
        
3. publish directory

        Note: Each plugin will describe the paths that needs to be placed in public directory, 
        the installer will create a symlink for that path in this format: 
        `public/vendor/plugin/ `
        
