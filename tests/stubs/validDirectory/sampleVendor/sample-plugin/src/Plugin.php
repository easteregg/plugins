<?php

namespace SampleVendor\PluginName;

class Plugin extends \Easteregg\Plugins\PluginBaseServiceProvider
{

    /**
     * Get the list of plugin service providers
     *
     * @return array
     */
    public function serviceProviders(): array
    {
        return [];
    }

    /**
     * Actions needed to take place when trying to activate a method
     */
    public function activate()
    {
        // TODO: Implement activate() method.
    }

    /**
     * Actions needed to take place when trying to deactivate a method
     */
    public function deactivate()
    {
        var_dump("deactivate method called");
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}
