<?php

namespace Tests;

use Easteregg\Plugins\PluginsServiceProvider;
use Orchestra\Testbench\BrowserKit\TestCase as TestBenchCase;

abstract class TestCase extends TestBenchCase
{
    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->app->setLocale('en');

        $this->migrateTables();

    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
        $app['config']->set('translatable.locales', ['en']);

        $app['config']->set('plugins.pre.path', __DIR__ . '/../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/');
        $app['config']->set('plugins.post.path', __DIR__ . '/../vendor/orchestra/testbench/fixture/storage/app/plugins/');
    }


    private function migrateTables()
    {
        $this->artisan('migrate', [
            '--database' => 'testing',
        ]);

        $this->artisan('vendor:publish');
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            PluginsServiceProvider::class
        ];
    }


}

