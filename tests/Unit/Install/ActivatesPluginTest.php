<?php

namespace Tests\Unit\Install;

use Easteregg\Plugins\Jobs\ActivatesPlugin;
use Tests\TestCase;

class ActivatesPluginTest extends TestCase
{

    /**
     * @test
     * @expectedException \Easteregg\Plugins\Exceptions\UnexpectedErrorException
     */
    public function it_should_check_if_the_path_is_valid_or_not()
    {
        $pluginPath = __DIR__ . '/../../';
        $job = new ActivatesPlugin($pluginPath);
    }

    /**
     * @test
     */
    public function it_should_copy_the_files_then_dump_the_autoload_for_further_requests()
    {
        // Make necessary variables.
        $validPlugin = __DIR__ . '/../../stubs/validDirectory/sampleVendor';
        @mkdir(__DIR__ . '/../../stubs/newValidDirectory');
        $testPath = __DIR__ . '/../../stubs/newValidDirectory/sampleVendor';
        exec("cp $validPlugin $testPath -rf");
        // Bind path to fixture.
        $oldStorage = storage_path();
        $oldBase = base_path();
        $this->app->setBasePath(__DIR__ . '/../../../');
        $this->app->bind('path.storage', function () {
            return __DIR__ . '/../../../fixture/storage';
        });
        $this->app['config']->set('plugins.post.path', __DIR__ . '/../../../fixture/storage/app/plugins/');

        // RUN THE TEST
        $job = new ActivatesPlugin($testPath . '/sample-plugin/plugin.json');
        $path = $job->handle();
        $this->assertEquals(storage_path('app/plugins/SampleVendor/PluginName'), $path);
        exec("rm " . __DIR__ . '/../../stubs/newValidDirectory -rf');
        exec("rm " . storage_path("app/plugins/SampleVendor") . " -rf");

        $this->app->bind('path.storage', function () use($oldStorage) {
            return $oldStorage;
        });

        $this->app->setBasePath($oldBase);
    }

    /**
     * @test
     */
    public function it_should_create_destination_if_it_does_not_exist()
    {
        // making backup of stubs

        $path = __DIR__ . '/../../../fixture/non-existent/';
        $this->app['config']->set('plugins.post.path', $path);
        $pathToCopy = __DIR__ . '/../../stubs/validDirectory/sampleVendor';
        @mkdir(__DIR__ . '/../../stubs/validDirectoryBackup');
        @mkdir(__DIR__ . '/../../stubs/validDirectoryBackup/sampleVendor');
        @exec("cp $pathToCopy/sample-plugin " . __DIR__ . '/../../stubs/validDirectoryBackup/sampleVendor/ -rf');
        // Test started here.
        $job = new ActivatesPlugin(__DIR__ . "/../../stubs/validDirectoryBackup/sampleVendor/sample-plugin/plugin.json");
        $job->handle();
        $this->assertEquals($path, $job->getDestinationPath());


        // removing created files.
        @exec("rm " . __DIR__ . '/../../../fixture/non-existent -rf');
        @exec("rm " . __DIR__ . '/../../stubs/validDirectoryBackup -rf');
    }

    /**
     * @test
     */
    public function it_should_fetch_the_destination_path_from_config()
    {
        $path = __DIR__ . '/../';
        $this->app['config']->set('plugins.post.path', $path);
        $job = new ActivatesPlugin(__DIR__ . '/../../stubs/validDirectory/sampleVendor/sample-plugin/plugin.json');

        $this->assertEquals($path, $job->getDestinationPath());
    }

    /**
     * @test
     */
    public function it_should_get_the_path_to_plugin_json()
    {
        $pluginPath = __DIR__ . '/../../stubs/validDirectory/sampleVendor/sample-plugin/plugin.json';
        $job = new ActivatesPlugin($pluginPath);
        $this->assertEquals($pluginPath, $job->getPluginJsonPath());
    }

    /**
     * @test
     */
    public function it_should_move_the_plugin_into_destination_according_to_its_vendor_and_name()
    {
        // Make necessary variables.
        $validPlugin = __DIR__ . '/../../stubs/validDirectory';
        $testPath = __DIR__ . '/../../stubs/newValidDirectory';
        exec("cp $validPlugin $testPath -rf");

        // create the job
        $this->app['config']->set('plugins.post.path', storage_path('app/plugins/'));
        $job = new ActivatesPlugin($testPath . '/sampleVendor/sample-plugin/plugin.json');

        $path = $job->handle();
        $this->assertDirectoryExists(storage_path("app/plugins/SampleVendor/PluginName"));
        $this->assertEquals($path, storage_path("app/plugins/SampleVendor/PluginName"));

        exec("rm $testPath -rf");
        exec("rm " . storage_path("app/plugins") . " -rf");
    }

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();

        \Mockery::close();
    }

}
