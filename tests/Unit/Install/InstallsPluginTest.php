<?php

namespace Tests\Unit\Install;

use Easteregg\Plugins\Jobs\InstallsPlugin;
use Illuminate\Support\Collection;
use Tests\TestCase;

class InstallsPluginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }
    /**
     * @test
     */
    public function it_should_activate_a_plugin_json()
    {
        $path = __DIR__ . '/../../../fixture/storage/app/plugins/NewVendor/NewPlugin/plugin.json';

        $job = new InstallsPlugin($path);

        $this->assertEquals("activated", $job->handle());
    }
}
