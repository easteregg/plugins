<?php

namespace Tests\Unit\Install;

use Easteregg\Plugins\Jobs\DumpsAutoload;
use Illuminate\Support\Collection;
use Tests\TestCase;

class DumpsAutoloadTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind('all-active-plugins', function () {
            return new Collection();
        });
    }

    /**
     * @test
     */
    public function it_should_dump_autoload_on_production()
    {
        // Preparation

        $this->app->detectEnvironment(function () {
            return 'production';
        });

        exec("rm " . storage_path('app/plugins') . ' -rf');
        @mkdir(storage_path('app/plugins'));
        $path = __DIR__ . '/../../stubs/validDirectory/sampleVendor';
        $destination = storage_path('app/plugins');
        exec("cp $path $destination -rf");
        @rename($destination . '/sampleVendor', $destination . '/SampleVendor');
        exec("mv $destination/SampleVendor/sample-plugin $destination/SampleVendor/PluginName");

        $this->app->setBasePath(__DIR__ . '/../../../');
        // Execution
        $job = new DumpsAutoload();
        $job->handle();


        // Assertion
        $psr4 = require __DIR__ . '/../../../vendor/composer/autoload_psr4.php';
        $this->assertArrayHasKey('SampleVendor\\PluginName\\', $psr4);


        // Clean up
        exec("rm " . storage_path('app/plugins') . ' -rf');
        exec("rm $destination/". "SampleVendor" ." -rf");

        $job = new DumpsAutoload();

        $job->handle();
    }

    public function tearDown()
    {
        parent::tearDown();
        \Mockery::close();
    }
}
