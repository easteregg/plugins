<?php

namespace Tests\Unit\Compressed;

use Easteregg\Plugins\Extraction\Factory;
use Tests\TestCase;

class FactoryTest extends TestCase
{
    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_should_not_allow_direct_class_instantiation()
    {
        new Factory();
    }

    /**
     * @test
     * @expectedException \Easteregg\Plugins\Exceptions\InvalidFileException
     * @expectedExceptionMessage Invalid File provided for plugin.
     */
    public function it_should_throw_exception_if_a_path_to_invalid_compressed_file_provided()
    {
        Factory::make(__DIR__ . '/../../');
    }


}
