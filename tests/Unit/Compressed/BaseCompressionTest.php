<?php

namespace Tests\Unit\Compressed;

use Tests\TestCase;

abstract class BaseCompressionTest extends TestCase
{
    abstract public function getDriver();

    abstract protected function instantiate();
}
