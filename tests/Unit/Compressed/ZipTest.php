<?php

namespace Tests\Unit\Compressed;

use Easteregg\Plugins\Extraction\Zip;

class ZipTest extends BaseCompressionTest
{

    use TestImplementsValidation;

    /**
     * @return \Easteregg\Plugins\Contracts\Compressed
     */
    public function getDriver()
    {
        return Zip::class;
    }

    /**
     * @test
     */
    public function it_should_return_false_if_destination_doesnt_have_permission()
    {

        $zipMock = $this->getMockBuilder(\ZipArchive::class)
            ->setConstructorArgs([])
            ->getMock();
        $zipMock->method('extractTo')->willThrowException(new \ErrorException("Permission denied"));
        $zip = $this->instantiate();
        $zip->setZipArchive($zipMock);
        $zip->setTargetPath(__DIR__);
        $this->assertFalse($zip->extract());
        exec("rm " . storage_path("app/temp") . " -rf");
    }

    /**
     * @test
     */
    public function it_should_return_false_if_the_passed_file_is_invalid()
    {
        copy(__DIR__ . '/../../stubs/invalidVendor.zip', __DIR__ . '/../../stubs/invalidVendorBak.zip');
        $zip = new Zip(__DIR__ . '/../../stubs/invalidVendorBak.zip');
        $zip->setTargetPath(__DIR__ . '/../../stubs/');
        $this->assertFalse($zip->extract());
        exec("rm " . __DIR__ .'/../../stubs/Vendor' . " -rf");
    }

    /**
     * @test
     */
    public function it_should_remove_the_file_after_successful_extraction()
    {
        $zip = $this->instantiate();
        $zip->setTargetPath(__DIR__ . '/../../stubs/');

        $zip->extract();
        $this->assertFileNotExists(__DIR__ .'/../../stubs/VendorBakInstance.zip');
        exec("rm "  . __DIR__ .'/../../stubs/Vendor' . " -rf");
    }

    /**
     * @test
     */
    public function it_should_remove_the_file_after_fail_extraction()
    {
        copy(__DIR__ . '/../../stubs/invalidVendor.zip', __DIR__ . '/../../stubs/invalidVendorBak.zip');
        $zip = new Zip(__DIR__ . '/../../stubs/invalidVendorBak.zip');
        $zip->extract();

        $this->assertFileNotExists(__DIR__ . '/../../stubs/invalidVendorBak.zip');
        exec("rm " . storage_path("app") . " -rf");
    }

    /**
     * @return \Easteregg\Plugins\Extraction\Zip
     */
    protected function instantiate(): \Easteregg\Plugins\Extraction\Zip
    {
        copy(realpath(__DIR__ . '/../../stubs/Vendor.zip'), __DIR__ . '/../../stubs/VendorBakInstance.zip');
        $zip = new Zip(__DIR__ . '/../../stubs/VendorBakInstance.zip');

        return $zip;
    }

}
