<?php

namespace Tests\Unit\Compressed;

use Easteregg\Plugins\Contracts\Compressed;

trait TestImplementsValidation
{

    /**
     * @test
     */
    public function it_should_extract_to_the_given_path()
    {
        $instance = $this->instantiate();
        $instance->to(storage_path('app/plugins'));
        $this->assertTrue($instance->extract());
        $this->assertFileNotExists(__DIR__ .'/../../stubs/VendorBakInstance.zip');
        exec("rm " . storage_path("app") . " -rf");
    }


    /**
     * @test
     */
    public function it_should_implement_compressed_contract()
    {
        $this->assertTrue(array_key_exists(Compressed::class, class_implements($this->getDriver())));
    }

    /**
     * @test
     */
    public function it_should_set_destination()
    {
        $instance = $this->instantiate();
        $instance->to(__DIR__ .'/../../stubs/random');
        $this->assertEquals(__DIR__ . '/../../stubs/random', $instance->getDestination());
        @unlink(__DIR__ . '/../../stubs/VendorBakInstance.zip');
    }

    /**
     * @test
     */
    public function it_should_provide_fluid_interface_for_to_method()
    {
        $instance = $this->instantiate();
        $res = $instance->to(__DIR__ .'/../../stubs/random');
        $this->assertInstanceOf($this->getDriver(), $res);
        @unlink(__DIR__ . '/../../stubs/VendorBakInstance.zip');
    }


    /**
     * @test
     */
    public function it_should_get_the_source_path()
    {
        $instance = $this->instantiate();
        $this->assertEquals(__DIR__ . '/../../stubs', $instance->getPath());
        @unlink(__DIR__ . '/../../stubs/VendorBakInstance.zip');
    }

    /**
     * @test
     */
    public function it_should_get_the_file_name_from_path()
    {
        $instance = $this->instantiate();
        $this->assertEquals("VendorBakInstance", $instance->getFileName());
        @unlink(__DIR__ . '/../../stubs/VendorBakInstance.zip');
    }
}
