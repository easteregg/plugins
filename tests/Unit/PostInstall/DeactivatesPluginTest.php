<?php

namespace Tests\Unit\PostInstall;

use Easteregg\Plugins\Entities\PluginInformation;
use Easteregg\Plugins\Exceptions\UnexpectedErrorException;
use Easteregg\Plugins\Jobs\DeactivatesPlugin;
use Illuminate\Support\Collection;
use Tests\TestCase;

class DeactivatesPluginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }

    /**
     * @test
     */
    public function it_should_deactivate_the_plugin()
    {
        // Copy new vendor to backup location
        $activePlugin = __DIR__ . '/../../../fixture/storage/app/plugins/NewVendor';
        @mkdir(storage_path('app/plugins'), 0755, true);

        $autoload = config("plugins.post.path");
        exec("cp {$activePlugin} {$autoload}NewVendor -rf" );

        // Set the default pre path to this location:
//        $this->app['config']->set("plugins.pre.path", __DIR__ . '/../../../fixture/storage/app/temp/plugins/');


        $job = new DeactivatesPlugin(storage_path('app/plugins/NewVendor/NewPlugin/plugin.json'));
        $result = $job->handle();
        $this->assertEquals("deactivated", $result);
        $this->assertDirectoryExists(storage_path('app/temp/plugins/NewVendor/NewPlugin'));


        // remove the newly created inactive directory
        exec("rm ". storage_path("app/temp")." -rf", $output);
        exec("rm ". storage_path("app/plugins")." -rf", $output);
    }

    /**
     * @test
     */
    public function it_should_set_the_plugin()
    {
        $job = new DeactivatesPlugin(__DIR__ . '/../../stubs/validDirectory/sampleVendor/sample-plugin/plugin.json');

        $job->setPlugin(new PluginInformation(__DIR__ . '/../../../fixture/storage/app/plugins/NewVendor/NewPlugin/plugin.json'));
        $this->assertEquals(__DIR__ . '/../../../fixture/storage/app/plugins/NewVendor/NewPlugin',
            $job->getPlugin()->getAttribute('path'));
    }

    /**
     * @test
     */
    public function it_should_throw_exception_if_cannot_move_the_plugin()
    {
        $pathToPlugin = __DIR__ . '/../../../fixture/storage/app/plugins/NewVendorBackup';
        exec('cp ' .
            __DIR__ . '/../../../fixture/storage/app/plugins/NewVendor '
            .
            $pathToPlugin . ' -rf');

        $this->app['config']->set("plugins.pre.path", '/');

        try {
            $job = new DeactivatesPlugin($pathToPlugin . '/new-plugin/plugin.json');
            $job->handle();
        } catch (UnexpectedErrorException $exception) {
            exec("rm $pathToPlugin -rf", $output);
            exec("rm " . __DIR__ . '/../../../fixture/storage/app/temp/plugins/new-plugin' . " -rf");

            // this should work. so we assert to get a green light.
            $this->assertTrue(true);
            return 0;
        }

        $this->fail('Expected an exception, but throws nothing.');


    }

}
