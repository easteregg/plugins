<?php

namespace Tests\Unit;

use Easteregg\Plugins\Entities\PluginInformation;
use Illuminate\Support\Collection;
use SampleVendor\PluginName\Plugin;
use Tests\TestCase;

class PluginInformationTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }
    /**
     * @test
     */
    public function it_should_create_object_with_the_array_of_key_values()
    {
        $pi = new PluginInformation([
            "name" => 'test'
        ]);

        $this->assertEquals('test', $pi->getAttribute('name'));
    }

    /**
     * @test
     */
    public function it_should_populate_the_plugin_object_if_the_path_to_plugin_json_provided()
    {
        $pi = new PluginInformation(__DIR__ . '/../stubs/validDirectory/sampleVendor/sample-plugin/plugin.json');

        $this->assertEquals("plugin-name", $pi->getAttribute('name'));
        $this->assertEquals("SampleVendor\\PluginName\\", $pi->getAttribute('autoload'));
    }
    
    /**
     * @test
     */
    public function it_should_instantiate_a_plugin_with_the_information()
    {
        $pi = new PluginInformation(__DIR__ . '/../stubs/validDirectory/sampleVendor/sample-plugin/plugin.json');
        require __DIR__ . '/../stubs/validDirectory/sampleVendor/sample-plugin/src/Plugin.php';
        $this->app->bind("SampleVendor\\PluginName\\Plugin", function () {
            return new Plugin($this->app);
        });


        $this->assertInstanceOf(Plugin::class, $pi->getInstance());
    }

}
