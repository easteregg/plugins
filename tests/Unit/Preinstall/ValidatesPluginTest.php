<?php

namespace Tests\Unit\Preinstall;

use Easteregg\Plugins\Jobs\ValidatesPlugin;
use Illuminate\Support\Collection;
use Tests\TestCase;

class ValidatesPluginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }
    /**
     * @test
     */
    public function it_returns_boolean_when_handled()
    {
        exec("cp " . __DIR__ . '/../../stubs/invalidDirectory ' . __DIR__ .'/../../stubs/invalidDirectoryBackup -rf');
        $job = new ValidatesPlugin(__DIR__ . '/../../stubs/validDirectory');

        $this->assertTrue($job->handle());

        $job2 = new ValidatesPlugin(__DIR__ . '/../../stubs/invalidDirectoryBackup');

        $result = $job2->handle();

        $this->assertFalse($job2->handle());
    }

    /**
     * @test
     */
    public function it_should_remove_the_invalid_plugin_directory()
    {
        exec('cp ' . __DIR__ . '/../../stubs/invalidDirectory ' . __DIR__ .'/../../stubs/invalidDirectoryBackup -rf');
        $job = new ValidatesPlugin(__DIR__ . '/../../stubs/invalidDirectoryBackup');

        $job->handle();

        $this->assertFileNotExists(__DIR__ . '/../../stubs/invalidDirectoryBackup');
    }
    
    /**
     * @test
     */
    public function it_should_check_if_the_plugin_provides_plugin_provider()
    {
        exec('cp ' . __DIR__ . '/../../stubs/validDirectory/sampleVendorWithoutPlugin ' .
            __DIR__ . '/../../stubs/validDirectory/sampleVendorWithoutPluginBackup -rf');
        $job = new ValidatesPlugin(__DIR__ . '/../../stubs/validDirectory/sampleVendorWithoutPluginBackup');

        $result = $job->handle();
        $this->assertFalse($result);
        $this->assertDirectoryNotExists(__DIR__ . '/../../stubs/validDirectory/sampleVendorWithoutPluginBackup');
    }
}
