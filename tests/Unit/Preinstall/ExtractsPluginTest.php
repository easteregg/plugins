<?php

namespace Tests\Unit\Preinstall;

use Easteregg\Plugins\Extraction\Zip;
use Easteregg\Plugins\Jobs\ExtractsPlugin;
use Illuminate\Support\Collection;
use Tests\TestCase;

class ExtractsPluginTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        @copy(__DIR__ . '/../../stubs/Vendor.zip', __DIR__ . '/../../stubs/VendorBak.zip');
        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }


    /**
     * @test
     */
    public function it_should_extract_the_file_to_temp_path()
    {
        $this->setZipDriver();
        $this->app['config']->set('plugins.pre.path', __DIR__ .'/../../stubs');

        $job = new ExtractsPlugin(__DIR__ . '/../../stubs/VendorBak.zip');
        $path = $job->handle();

        $this->assertEquals(__DIR__ . '/../../stubs/Vendor/Plugin', $path);
        $this->assertDirectoryExists(__DIR__ . '/../../stubs/Vendor/Plugin');
        // remove the extracted plugin folder
        $this->rrmdir($path);
    }

    /**
     * @test
     */
    public function it_should_find_the_zip_driver()
    {
        $this->setZipDriver();

        $job = new ExtractsPlugin(__DIR__ . '/../../stubs/Vendor.zip');
        $this->assertInstanceOf(Zip::class, $job->getPlugin());
    }


    protected function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function tearDown()
    {
        parent::tearDown();

        @unlink(realpath(__DIR__ . '/../../stubs/vendorBak.zip'));
    }

    protected function setZipDriver()
    {
        $this->app['config']->set('plugins.pre.drivers', [
            'zip' => Zip::class
        ]);
    }
}
