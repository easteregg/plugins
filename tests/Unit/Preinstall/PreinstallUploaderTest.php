<?php

namespace Tests\Unit\Preinstall;

use Easteregg\Plugins\Jobs\UploadsPlugin;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\TestCase;

class PreinstallUploaderTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        copy(__DIR__ . '/../../stubs/Vendor.zip', __DIR__ . '/../../stubs/VendorBak.zip');
        $this->app->bind("all-active-plugins", function () {
            return new Collection();
        });
    }
    /**
     * @test
     */
    public function it_should_get_the_uploaded_file_and_move_it_with_a_new_name_to_temp()
    {
        $uploadedFile = $this->makeUploadedFile();

        $uploader = new UploadsPlugin($uploadedFile);

        $result = $uploader->handle();

        $this->assertFileExists($result);

        exec("rm " . storage_path('app/temp') . ' -rf');
    }

    private function makeUploadedFile()
    {
        return new UploadedFile(
            realpath(__DIR__ .'/../../stubs/Vendor.zip'),
            'Vendor.zip',
            'application/zip',
            filesize(__DIR__ .'/../../stubs/Vendor.zip'),
            null,
            true
        );
    }

    public function tearDown()
    {
        parent::tearDown();

        rename(__DIR__ . '/../../stubs/VendorBak.zip', __DIR__ . '/../../stubs/Vendor.zip');
    }
}


