<?php

namespace Tests\Unit\Preinstall;

use Easteregg\Plugins\Jobs\FetchesPluginInformation;
use Illuminate\Support\Collection;
use Tests\TestCase;

class AddsPluginToNotInstalledListTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        cache()->forever("all-active-plugins", new Collection());
    }
    /**
     * @test
     */
    public function it_should_fetch_plugin_information()
    {
        $job = new FetchesPluginInformation(__DIR__ . '/../../stubs/validDirectory');

        $plugins = $job->handle();

        $this->assertEquals(2, $plugins->count());
    }
}
