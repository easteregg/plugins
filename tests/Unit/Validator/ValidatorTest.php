<?php

namespace Tests\Unit\Validator;

use Easteregg\Plugins\Contracts\Validator;
use Easteregg\Plugins\Facades\PluginValidator;
use Illuminate\Support\Collection;
use Tests\TestCase;

class ValidatorTest extends TestCase
{
    protected $stubPath = __DIR__ . '/../../stubs';

    protected $stubFile = __DIR__ . '/../../stubs/Vendor.zip';

    protected $validPath = __DIR__ . '/../../stubs/validDirectory';

    protected $invalidPath = __DIR__ . '/../../stubs/invalidDirectory';

    public function setUp()
    {
        parent::setUp();
        exec("cp " . $this->invalidPath . ' ' . $this->invalidPath . 'Backup -rf');
        $this->invalidPath = $this->invalidPath . 'Backup';
        $this->app->bind('all-active-plugins', function () {
            return new Collection();
        });
    }

    public function tearDown()
    {
        parent::tearDown();

        exec("rm {$this->invalidPath} -rf");
    }


    /**
     * @test
     */
    public function it_should_create_an_instance_based_on_a_path()
    {
        $validator = PluginValidator::make($this->stubPath);

        $this->assertInstanceOf(Validator::class, $validator);
        $this->assertEquals($this->stubPath, $validator->getAttribute('src'));
    }

    /**
     * @test
     */
    public function it_should_return_null_if_accessing_a_not_existent_attribute()
    {
        $validator = PluginValidator::make($this->stubPath);
        $this->assertEquals(null, $validator->getAttribute('not-here'));
    }

    /**
     * @test
     */
    public function it_should_the_existence_of_given_path()
    {
        $validator = PluginValidator::make($this->stubPath);
        $this->assertTrue($validator->passes());
        $this->assertFalse($validator->fails());
        $failedValidation = PluginValidator::make(__DIR__ . '/../../non-existent/file');
        $this->assertFalse($failedValidation->passes());
    }

    /**
     * @test
     */
    public function it_should_check_if_the_given_path_is_a_directory()
    {
        $validator = PluginValidator::make($this->stubPath);
        $this->assertTrue($validator->passes());
        $failedValidation = PluginValidator::make($this->stubFile);
        $this->assertFalse($failedValidation->passes());
    }

    /**
     * @test
     */
    public function it_should_check_if_the_given_path_contains_a_valid_plugin_json()
    {
        $validator = PluginValidator::make($this->validPath);

        $this->assertTrue($validator->passes());

        $this->assertFalse(PluginValidator::make($this->invalidPath)->passes());

    }
}
