<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class InactiveThemesControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_should_provide_a_list_of_inactive_themes()
    {
        $this->visit('dashboard/plugins/inactive-themes')
            ->assertResponseOk();
    }
}
