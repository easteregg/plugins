<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\PluginInformationGenerator;
use Tests\TestCase;

class InactivePluginsControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_should_load_a_message_if_there_is_no_inactive_plugin_found()
    {
        $this->visit('dashboard/plugins/inactive-plugins')
            ->see(trans("plugins::messages.noInactivePluginFound"));
    }

    /**
     * @test
     */
    public function it_should_load_inactive_plugins()
    {
        $plugins = $this->makeInactivePlugins(10);
        $this->visit("dashboard/plugins/inactive-plugins")
            ->see($plugins->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function it_should_have_a_hidden_form_for_each_inactive_plugin_to_activate_it()
    {
        $plugins = $this->makeInactivePlugins(2);
        $this->visit('dashboard/plugins/inactive-plugins')
            ->seeElement("form", [
                'method' => 'post',
                'action' => url("dashboard/plugins/activate"),
            ])
            ->seeElement("input", [
                'type' => 'hidden',
                'name' => "_token",
            ])
            ->seeElement("input", [
                'type'  => 'hidden',
                'name'  => 'plugin',
                'value' => $plugins->first()->getAttribute('key'),
            ])
            ->seeElement("input", [
                'name' => 'plugin',
                'type' => 'hidden',
                'value' => $plugins->last()->getAttribute('key')
            ]);
    }

    /**
     * @test
     */
    public function it_should_have_a_back_to_plugins_button()
    {
        $this->visit('dashboard/plugins/inactive-plugins')
            ->seeLink(trans("plugins::messages.backToPluginManagement"));
    }

    protected function makeInactivePlugins($count = 2)
    {
        $plugins = PluginInformationGenerator::make($count, "plugin");
        cache()->forever('inactive-plugins', $plugins);

        return $plugins;
    }

}
