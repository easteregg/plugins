<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\PluginInformationGenerator;
use Tests\TestCase;

class ActivePluginsControllerTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * @test
     */
    public function it_should_load_active_plugins()
    {
        $this->visit("dashboard/plugins/active-plugins")
            ->see(trans("plugins::messages.activePlugins"));
    }

    /**
     * @test
     */
    public function it_should_provide_a_form_to_deactivate_items()
    {
        $plugins = $this->makeActivePlugins(2);

        $this->visit('dashboard/plugins/active-plugins')
            ->see($plugins->first()->getAttribute('key'))
            ->see($plugins->last()->getAttribute('key'));
    }

    /**
     * @test
     */
    public function it_should_provide_a_publish_button_to_publish_plugin_assets()
    {
        $plugins = $this->makeActivePlugins(2);

        $this->visit('dashboard/plugins/active-plugins')
            ->seeElement('form', [
                'method' => "post",
                'action' => url("dashboard/plugins/publish"),
            ])
            ->seeElement('input', [
                'type'  => 'hidden',
                'name'  => 'plugin',
                'value' => $plugins->first()->getAttribute('key'),
            ])
            ->seeElement('button', [
                'type' => 'submit'
            ])
            ->seeElement('input', [
                'type'  => 'hidden',
                'name'  => '_token',
                'value' => '',
            ]);
    }



    protected function makeActivePlugins($count = 2)
    {
        $plugins = PluginInformationGenerator::make($count, "plugin");

        cache()->forever('active-plugins', $plugins);

        return $plugins;
    }
}
