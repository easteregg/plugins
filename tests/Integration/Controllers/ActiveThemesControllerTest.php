<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ActiveThemesControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_should_provide_a_list_of_active_themes()
    {
        $this->visit('dashboard/plugins/active-themes')
            ->assertResponseOk();
    }
}
