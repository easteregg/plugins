<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Mockery;
use Tests\TestCase;

class PublishesPluginAssetsController extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_should_publish_a_plugin()
    {
        $this->app->bind("path.storage", function () {
            return __DIR__ . '/../../../fixture/storage/';
        });
        $this->app['config']->set('plugins.post.path', storage_path('app/plugins/'));

        $m = Mockery::mock('Easteregg\Plugins\Jobs\InstallsPlugin')
            ->shouldReceive('handle')
            ->once()
            ->andReturn(null)
            ->getMock();

        $this->app->bind('Easteregg\Plugins\Jobs\InstallsPlugin', function () use ($m) {
            return $m;
        });

        $this->post('dashboard/plugins/publish', [
            'plugin' => "NewVendor/NewPlugin",
        ])->assertRedirect('dashboard/plugins/active-plugins');
    }

    public function tearDown()
    {
        parent::tearDown();

        Mockery::close();
    }
}
