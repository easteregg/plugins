<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\PluginInformationGenerator;
use Tests\TestCase;

class PluginsControllerTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();
        $this->app['config']->set("plugins.dashboardLayout", "plugins::layout");
    }

    /**
     * @test
     */
    public function it_should_not_error_when_no_plugin_is_there()
    {
        // Preparation
        $this->app->bind('path.storage', function () {
            return __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage';
        });

        $this->app['config']->set("plugins.post.path", storage_path("app/plugins"));

        exec("rm " . config("plugins.post.path") . " -rf");
        exec("rm " . config("plugins.pre.path") . " -rf");


        // Execution
        $this->visit('dashboard/plugins')
            ->see(trans("plugins::messages.pluginManagement"))
            ->seeLink("0 " . trans("plugins::messages.activePlugins"), url("dashboard/plugins/active-plugins"))
            ->see("0 " . trans("plugins::messages.inactivePlugins"));
    }

    /**
     * @test
     */
    public function it_should_load_plugins_count()
    {
        $plugins = $this->makeActivePlugins(10);

        $onlyPlugins = $plugins->filter(function ($plugin) {
            return $plugin->getAttribute("type") == "plugin";
        });

        cache()->forever('active-plugins', $onlyPlugins);

        $onlyPlugins = $onlyPlugins->count();

        $this->visit("dashboard/plugins")
            ->see("$onlyPlugins " . trans("plugins::messages.activePlugins"));
    }

    /**
     * @test
     */
    public function it_should_load_active_themes_count()
    {
        $plugins = $this->makeActivePlugins(10);
        $onlyThemes = $plugins->filter(function ($plugin) {
            return $plugin->getAttribute("type") == "theme";
        });
        cache()->forever('active-themes', $onlyThemes);

        $onlyThemes = $onlyThemes->count();

        $this->visit('dashboard/plugins')
            ->see("$onlyThemes " . trans("plugins::messages.activeThemes"));
    }

    /**
     * @test
     */
    public function it_should_show_a_form_to_upload_plugin()
    {
        $this->visit("dashboard/plugins")
            ->seeElement('form', [
                'action'  => url("dashboard/plugins"),
                'enctype' => 'multipart/form-data',
            ])
            ->seeElement('input', [
                'type' => 'hidden',
                'name' => "_token",
            ])
            ->seeELement('input', [
                'type'  => 'file',
                'name'  => 'plugin',
                'value' => '',
            ]);
    }

    /**
     * @test
     */
    public function it_should_validate_uploaded_plugin_to_be_in_form_of_acceptable_value()
    {
        $this->makeProperDestination();

        $response = $this->call('POST', 'dashboard/plugins', [], [], [
            'plugin' => $this->makeFile(),
        ]);

        $this->assertSessionMissing('errors');

        $this->removeExtractedFiles();
    }

    /**
     * @test
     */
    public function it_should_dump_autoload_plugin()
    {
        $this->makeProperDestination();

        $response = $this->call('POST', 'dashboard/plugins', [], [], [
            'plugin' => $this->makeFile(),
        ]);

        $this->removeExtractedFiles();
    }


    protected function makeActivePlugins($count = 1)
    {
        $plugins = PluginInformationGenerator::make($count);
        cache()->forever('all-active-plugins', $plugins);

        return $plugins;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    protected function makeFile()
    {
        exec("cp " . __DIR__ . '/../../stubs/Vendor.zip ' . __DIR__ . '/../../stubs/VendorUploadBak.zip');
        $sampleFilePath = realpath(__DIR__ . '/../../stubs/VendorUploadBak.zip');

        return new UploadedFile(
            $sampleFilePath,
            'VendorUploadBak.zip',
            mime_content_type(__DIR__ . '/../../stubs/VendorUploadBak.zip'),
            filesize(__DIR__ . '/../../stubs/VendorUploadBak.zip'),
            null,
            true
        );
    }

    private function makeProperDestination()
    {
        $this->app['config']->set('plugins.pre.path', storage_path('app/temp/plugins'));
    }

    private function removeExtractedFiles()
    {
        exec("rm " . storage_path("app/temp/plugins/Vendor") . " -rf");
    }
}
