<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class DeactivatesPluginControllerTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * @test
     */
    public function it_should_deactivate_a_plugin_with_a_post_request()
    {
        // Preparation
        @mkdir( __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/plugins/', 0755, true);
        @mkdir( __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/', 0755, true);

        exec("cp " . __DIR__ . '/../../../fixture/storage/app/plugins/NewVendor '
            . __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/plugins/ -rf'
        );

        $this->app['config']->set("plugins.post.path",
            __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/plugins/'
        );

        $this->app['config']->set("plugins.pre.path",
            __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/'
        );

        $plugin = 'NewVendor/NewPlugin';


        // Execution
        $this->post('dashboard/plugins/deactivate', [
            'plugin' => $plugin,
        ])
            ->assertRedirectedTo("dashboard/plugins");

        // Clean up
        exec("rm " . __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/plugins -rf');
        exec("rm " . __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins -rf');
    }
}
