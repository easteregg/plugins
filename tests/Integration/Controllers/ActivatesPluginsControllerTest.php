<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ActivatesPluginsControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_only_gets_the_path_to_the_directory_containing_plugin_json()
    {
        // Preparation
        @mkdir( __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/', 0755, true);
        exec("cp " . __DIR__ . '/../../../fixture/storage/app/plugins/NewVendor '
            . __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/ -rf'
        );

        $this->app['config']->set("plugins.pre.path",
            __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/temp/plugins/'
        );

        $plugin = 'NewVendor/NewPlugin';


        // Execute and assertions
        $redirect = $this->post('dashboard/plugins/activate', [
            'plugin' => $plugin,
        ]);
        $redirect->assertRedirectedTo('dashboard/plugins/install');


        // clean up
        exec("rm " . __DIR__ . '/../../../vendor/orchestra/testbench/fixture/storage/app/* -rf');
    }
}
