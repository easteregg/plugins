<?php

namespace Tests\Integration\Controllers;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class InstallsPluginControllerTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * @test
     */
    public function it_should_guide_users_to_install_plugin_assets()
    {
        $this->visit('dashboard/plugins/install')
            ->see(url('dashboard/plugins/active-plugins'));
    }
}
