<?php

namespace Tests;

use Easteregg\Plugins\Entities\PluginInformation;
use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;

class PluginInformationGenerator
{
    protected static $pluginOrTheme = [
        'plugin',
        'theme',
    ];

    public static function make($count = 1, $type = 'rand')
    {
        if ($type == "rand") {
            $type = static::$pluginOrTheme[rand(0, 1)];
        }

        if ($count == 1) {
            $faker = \Faker\Factory::create();
            $pluginInformation = new PluginInformation([
                'vendor'   => $vendor = $faker->slug, // vendor section of plugin.json name field
                'name'     => $faker->name, // name section of plugin.json name field
                'type'     => static::$pluginOrTheme[rand(0, 1)], // defaults to plugin,
                'path'     => storage_path('app/plugins'),
                'version'  => implode('.', [rand(1, 10), rand(1, 10), rand(1, 10)]),
                'autoload' => studly_case($vendor),
            ]);

            return $pluginInformation;
        }

        $collection = new Collection();
        for ($i = 0; $i < $count; ++$i) {
            $collection->push(static::make());
        }

        return $collection;
    }
}
